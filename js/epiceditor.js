(function (Drupal, EpicEditor, $) {

"use strict";

Drupal.editors.epiceditor = {

  attach: function (element, format) {
    var $element = $(element);
    var $target = $element;
    var containerId = $element.attr('id') + '-epiceditor';
    var defaultContent = $target.val();
    $target.hide().after('<div id="' + containerId + '" />');

    console.log(containerId);
    console.log(defaultContent);

    var opts = {
      container: containerId,
//      textarea: element.attr('#id'),
      basePath: '/modules/epiceditor/vendor/EpicEditor/epiceditor',
      clientSideStorage: false,
      useNativeFullscreen: true,
      parser: marked,
      file: {
        defaultContent: defaultContent
      },
      theme: {
        base: '/themes/base/epiceditor.css',
        preview: '/themes/preview/preview-light.css',
        editor: '/themes/editor/epic-light.css'
      },
      button: {
        preview: true,
        fullscreen: true
      },
      shortcut: {
        modifier: 18,
        fullscreen: 70,
        preview: 80
      },
      string: {
        togglePreview: 'Toggle Preview Mode',
        toggleEdit: 'Toggle Edit Mode',
        toggleFullscreen: 'Enter Fullscreen'
      }
    };
    var editor = new EpicEditor(opts).load();
    $target.data('epiceditor', editor);
  },

  detach: function (element, format, trigger) {
    var $element = $(element);
    var $target = $element;

    var editor = $target.data('epiceditor');

    $target.val(editor.exportFile());

    editor.unload(function () {
      $target.show();
    });
  },

  onChange: function (element, callback) {

  },

  attachInlineEditor: function (element, format, mainToolbarId, floatedToolbarId) {

  }
};

Drupal.epiceditor = {

}

})(Drupal, EpicEditor, jQuery);
