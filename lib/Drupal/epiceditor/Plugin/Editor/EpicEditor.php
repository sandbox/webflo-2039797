<?php

/**
 * @file
 * Contains \Drupal\epiceditor\Plugin\Editor\EpicEditor.
 */

namespace Drupal\epiceditor\Plugin\Editor;

use Drupal\Core\Language\Language;
use Drupal\editor\Plugin\EditorBase;
use Drupal\editor\Annotation\Editor;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\editor\Plugin\Core\Entity\Editor as EditorEntity;

/**
 * Defines a EpicEditor-based text editor for Drupal.
 *
 * @Editor(
 *   id = "epiceditor",
 *   label = @Translation("EpicEditor"),
 *   supports_inline_editing = FALSE
 * )
 */
class EpicEditor extends EditorBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return array(
      'toolbar' => array(
        'buttons' => array(
          array(
            'Bold', 'Italic',
            '|', 'DrupalLink', 'DrupalUnlink',
            '|', 'BulletedList', 'NumberedList',
            '|', 'Blockquote', 'DrupalImage',
            '|', 'Source',
          ),
        ),
      ),
      'plugins' => array(),
    );
  }

  function getJSSettings(EditorEntity $editor) {
  }

  /**
   * {@inheritdoc}
   */
  function getLibraries(EditorEntity $editor) {
    $libraries = array(
      array('epiceditor', 'drupal.epiceditor'),
    );
    return $libraries;
  }

  public function settingsForm(array $form, array &$form_state, EditorEntity $editor) {
    $form = parent::settingsForm($form, $form_state, $editor);
    $form['useNativeFullscreen'] = array(
      '#type' => 'checkbox',
      '#title' => 'useNativeFullscreen',
      '#default_value' => $editor->settings['useNativeFullscreen'],
    );
    return $form;
  }

  public function settingsFormSubmit(array $form, array &$form_state) {
    parent::settingsFormSubmit($form, $form_state);
  }

}
